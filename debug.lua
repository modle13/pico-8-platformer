-- print debug info to the screen
function draw_debug()
    local _cpu = stat(1)
    local _mem = stat(0)
    local _cpu_c = 11
    local _mem_c = 11

    if (_mem < _min_mem) _min_mem = _mem
    if (_mem > _max_mem) _max_mem = _mem

    if (_cpu > 0.8) _cpu_c = 8
    if (_mem > 250) _mem_c = 8

    print("cpu ".._cpu,0,8,_cpu_c)
    -- print("cpu ".._cpu,0,8,_cpu_c)
    print("mem ".._mem,0,16,_mem_c)
    print("mem min ".._min_mem,0,24,11)
    print("mem max ".._max_mem,0,32,11)
    print("player "..player1:getx(),0,40,11)
    --print("player: "..player1.x..","..player1.y,0,40)
    --print("enemy: "..bad1.x..","..bad1.y,0,48)
end
