pickupspawns={}

function resetpickups()
    for v in all(pickupspawns) do
        data = split(v)
        -- reset the sprite in the map
        mset(data[1], data[2], data[3])
        -- delete from spawn data
        del(pickupspawns, v)
    end
end
