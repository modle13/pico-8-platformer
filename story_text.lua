text_fg_color = 7
text_bg_color = 1

function draw_story_text()
    call_print("oh no!",                                globals.cell_width * 3, 108)
    call_print("shockbunny lost all",                   globals.cell_width * 10.5, 105)
    call_print("of his snacks!",                        globals.cell_width * 10.5, 113)
    call_print("press c to jump!",                      globals.cell_width * 4.5, 85)
    call_print("hold c to jump higher!",                globals.cell_width * 21, 12)
    call_print("you made it!",                          globals.cell_width * 72, 104)
    call_print("you found " .. tostring(player1.score), globals.cell_width * 80, 104)
    call_print("of shockbunny's snacks!",               globals.cell_width * 88, 104)
    -- call_print("boss battle",                           globals.cell_width * 98, 56)
    -- call_print("this way >>>",                          globals.cell_width * 98, 64)
    -- call_print("insert coin",                           globals.cell_width * 98, 76)
    -- call_print("to continue",                           globals.cell_width * 98, 84)
end

function call_print(text, x, y)
    -- this makes the text stationary
    ctprint(text, x, y, text_fg_color, text_bg_color)
end
