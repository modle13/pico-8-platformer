function init_gradient()
    if not globals.gradient then return end
    --swap 0x30 for 0x3n where n is the color that will be swapped for the gradient
    poke(0x5f5f, 0x30)
    --put the gradient colors in the table below:
    -- ,2 is the palette index; 1 is the main pico8 palette
    -- 0x82 is a combination of 8th (white) and 2nd (dark blue) colors, alternated in strips
    pal({[0]=0x82,0x82,0x84,0x84,4,4,0x89,0x89,0x8e,0x8e,0x8f,0x8f,15,15,0x87,0x87},2)
    --blend lines
    memset(0x5f70, 0xaa, 16)
end
