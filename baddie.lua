baddie={}
baddies={}
-- {id,x,y}
--   split each to get {id,x,y} and update game map with that
baddiespawns={}
-- w,h,spr,speed,frms
baddiedata={[2]='1,1,32,1,2',[4]='4,3,240,0.5,1'}

-- create a baddie
function baddie:new(x, y, sp, w, h)
    local o={}
    setmetatable(o, self)
    self.__index = self
    o.x=x
    o.y=y
    o.w=w or 1
    o.h=h or 1
    o.dx=0
    o.dy=0
    o.spr=sp
    o.speed=1
    o.frms=2
    o.isfaceright=false
    o.bounce=true --do we turn around at a wall?
    o.bad=true
    return o
end

function baddie:draw()
    if self.isfaceright then
        anim(self, self.spr, self.frms, 6, true)
    else
        anim(self, self.spr, self.frms, 6, false)
    end
end

function baddie:move()
    -- if (true) return
    self.startx = self.x
    if self.isfaceright then
        self.dx = self.speed
    else
        self.dx = -self.speed
    end
    update_loc(self)
end

function baddie:update()
end

function spawn_baddies(plyrx)
    for y=0,16 do
        -- check map positions 10 behind and ahead of player
        -- maybe change this to a spawn_range var
        for x = (plyrx / 8) - 10, (plyrx / 8) + 10 do
            val = mget(x, y)
            -- if flag 2
            if fget(val, 2) then
                -- use baddiedata here to get w,h,spr,speed,frms and combine both ifs
                -- 32 is sprite id
                local bad = baddie:new(x * 8, y * 8, 32)
                add(baddies, bad)
                -- reset map sprite
                mset(x, y, 0)
                add(baddiespawns, bad.x / 8 .. ',' .. bad.y / 8 .. ',' .. bad.spr)
            end
            if fget(val, 4) then
                -- use baddiedata here to get w,h,spr,speed,frms and combine both ifs
                -- that's a boss
                w,h = 4,4
                -- 240 is sprite id
                local bad = baddie:new(x * 8, y * 8 - (h - 1) * 8, 240, w, h)
                bad.frms = 4
                bad.speed = 0.5
                add(baddies, bad)
                -- reset map sprite
                mset(x, y, 0)
                add(baddiespawns, bad.x / 8 .. ',' .. bad.y / 8 .. ',' .. bad.spr)
            end
        end
    end
end

function resetbaddies()
    baddies = {}
    for v in all(baddiespawns) do
        data = split(v)
        -- reset the sprite in the map
        mset(data[1], data[2], data[3])
        -- delete from spawn data
        del(baddiespawns, v)
    end
end
